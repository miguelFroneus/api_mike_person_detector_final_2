import os
from flask import Flask
from flask_restful import Api
from logging.config import dictConfig

from lib.resources import HealthCheck
from lib.person_detector.resources import PersonDetector

from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__)
api = Api(app, prefix='/person_detector')
app.logger.info('Starting App with ENVIRON: {}'.format(os.environ))

api.add_resource(HealthCheck, '/healthcheck')
api.add_resource(PersonDetector, '/predict')


if __name__ == "__main__":
    HOST = app.config.get('APP_HOST')
    PORT = app.config.get('APP_PORT')

    app.run(host=HOST, port=PORT)
