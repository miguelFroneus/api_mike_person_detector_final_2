# local package
#-e .

python-dateutil==2.8.0
# external requirements
Sphinx
coverage
awscli
flake8
python-dotenv>=0.5.1

# api
Flask>=1.0.3
Flask-RESTful>=0.3.7
uWSGI>=2.0.18
requests>=2.22.0
#logging

#repository
spacy==2.1.4  
numpy==1.16.4  
pandas
soundfile
librosa
helpers
matplotlib
sklearn
tensorflow
keras
pydub







