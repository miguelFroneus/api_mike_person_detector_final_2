api-payment-confirmation
==============================

A short description of the project.

Project Organization
------------

    ├── LICENSE
    │
    ├── README.md          <- The top-level README for developers using this project.
    │
    ├── docker-compose.yml <- Docker composer script to build image & container
    ├── Dockerfile         <- Dockerfile builder 
    │
    ├── app.py             <- Main RESTful app with the routes
    ├── lib                <- Libraries required. Here put the app resourses 
    │   ├── __init__.py    <- Makes lib a Python module
    │   └── resources.py   <- Common resources to use
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── tests              <- Scripts to run app tests 
    │   ├── __init__.py    <- Makes src a Python module
    │   └── test-01.py     <- Test 01 
    │
    ├── uwsgi.ini          <- UWSGI config to deploy api 
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
