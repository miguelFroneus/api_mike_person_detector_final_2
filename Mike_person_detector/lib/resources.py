from flask_restful import Resource, reqparse
from flask import jsonify

import json

class HealthCheck(Resource):
    def get(self):
        return jsonify({'status':'OK'})

