
import pandas as pd

import numpy as np

#import IPython
from librosa.display import specshow

# Load various imports 
import pandas as pd
import os
import librosa
import librosa.display

import struct

import helpers

#import IPython

#import matplotlib as plt
#import seaborn

import matplotlib.pyplot as plt
from librosa.display import specshow

import keras

from sklearn.preprocessing import LabelEncoder
from keras.utils import to_categorical

from sklearn.model_selection import train_test_split 

import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.optimizers import Adam
from keras.utils import np_utils
from sklearn import metrics 

from keras.callbacks import ModelCheckpoint 
from datetime import datetime 

from pydub import AudioSegment

from keras.models import model_from_json

import time


#logger = logging.getLogger(__name__)
#print('starting Mike(The Best) Person Detector model')




class Model_C(object):
     def __init__(self, m_json, m_h5):
        # load json and create model
        json_file = open(m_json,'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model3 = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model3.load_weights(m_h5)
        # Compile the model
        loaded_model3.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer='adam')
        self.model=loaded_model3      



Instacia_Modelo = Model_C("./lib/person_detector/model2.json", "./lib/person_detector/model2.h5")


def predict(newAudio_array,instancia_modelo):

    start_time = time.time()
    id_audio="tuhermana44"
    t1 =0 #Works in milliseconds
    t2 =2000

    file_name_2seg =str(id_audio)+".ogg"

    #newAudio = AudioSegment.from_wav(file_name) #lee el original

    newAudio=AudioSegment(newAudio_array, sample_width=2, frame_rate=8000, channels=1)#toma el array del audio y lo pasa a audio
    newAudio = newAudio[t1:t2] #lo corta
    newAudio.export(file_name_2seg, format="ogg") #guarda el cortado con un nuevo nombre "file_name_2seg"    
       
    audio, sample_rate = librosa.load(file_name_2seg, res_type='kaiser_fast') #lee el cortado que esta guardado con el nombre "file_name_2seg"      
    mfccs = librosa.feature.mfcc(y=audio, sr=sample_rate, n_mfcc=40) #extrae sus features
    mfccsscaled = np.mean(mfccs.T,axis=0) #extrae sus features

    x_audio=np.array(mfccsscaled.tolist())  #pongo el vector de features en una lista

    num_rows = 5
    num_columns = 8
    num_channels = 1

    x_audio_train = x_audio.reshape(1, num_rows, num_columns, num_channels) #hago el reshape de las features
    ynew = instancia_modelo.model.predict(x_audio_train) #predigo la clase del audio cortado

    #print("Probability_Vector:", ynew)

    if ynew[0][0]>ynew[0][1]:
        prediccion_clase="CONTESTADOR"
    else:
        prediccion_clase="PERSONA"
    
    duracion=time.time()-start_time
    
    return prediccion_clase, ynew, duracion 






