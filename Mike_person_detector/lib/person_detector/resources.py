from flask_restful import Resource, reqparse
from flask import jsonify
from .model import *
import numpy as np
from array import array


#Instacia_Modelo = Model_C("./lib/person_detector/model2.json", "./lib/person_detector/model2.h5")


class PersonDetector(Resource):
    def post(self):
        start_time = time.time()
        parser = reqparse.RequestParser()
        parser.add_argument('data_array', type=int, action='append', required=True)  
        data = parser.parse_args()
        arg=array('h', data.data_array)
        prediccion_clase, ynew, duracion = predict(arg,Instacia_Modelo)
        duracion2=time.time()-start_time
        result = {'prediction': prediccion_clase, 'prob_Contestador':str(ynew[0][0]), 'prob_Persona':str(ynew[0][1]), 'duration':str(duracion), 'duration2':str(duracion2)}
        return jsonify(result)



